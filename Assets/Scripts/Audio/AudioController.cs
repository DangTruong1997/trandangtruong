﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioController : MonoBehaviour {

    //public AudioClip BackGroundClip;
    public static AudioController Instance;

    //public AudioClip playerWalk;

    public AudioSource BackGroundSource;

    private void Awake()
    {
        Instance = this;
    }

    // Use this for initialization
    void Start () {
        BackGroundSource.Play();
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.A))
        {
            BackGroundSource.Stop();
        }
	}

    public void PauseAudioClick()
    {
        BackGroundSource.volume = 0;
    }

    public void PlayAudio(AudioClip clip)
    {
        BackGroundSource.PlayOneShot(clip);
    }
    public void PlayAudioClick()
    {
        BackGroundSource.volume = 1;
    }
}
