﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LV1Controller : MonoBehaviour {

    public static LV1Controller Instance;
    
    public GameObject Pause;
    public GameObject GameOver;
    public Text Score;
    public float _score;


    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        _score = 0;
        Pause.SetActive(false);
        GameOver.SetActive(false);
    }

    private void Update()
    {
        Score.text = _score.ToString();
    }

    public void bottonPause()
    {
        Time.timeScale = 0;
        Pause.SetActive(true);
        PlayerController.Instance.speedmovePlayer = 0;
    }

    public void bottonMenu()
    {
        SceneManager.LoadScene("SceneGame");
    }

    public void bottonRestart()
    {
        SceneManager.LoadScene("Level1");
    }

    public void  bottonResume()
    {
        Time.timeScale = 1;
        PlayerController.Instance.speedmovePlayer = PlayerController.Instance.speedMove;
        Pause.SetActive(false);
    }

}
