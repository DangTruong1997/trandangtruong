﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SawController : MonoBehaviour
{

    public float SpeedRound;
    public float SpeedMove;
    public GameObject SawChildren;
    private SpriteRenderer SawRenderer;

    void Start()
    {
        SawRenderer = GetComponentInChildren<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if(SawRenderer.isVisible)
        {
            SawChildren.transform.Rotate(0, 0, SpeedRound * Time.deltaTime);
            transform.Translate(Vector3.left * SpeedMove * Time.deltaTime);
        }
        if(transform.position.y<-5.5f)
        {
            Destroy(gameObject);
        }
    }

    public void SawDie()
    {
        Destroy(gameObject);
    }
}
