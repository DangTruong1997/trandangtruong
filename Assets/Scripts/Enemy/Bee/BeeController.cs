﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeController : MonoBehaviour
{

    public Animator anim;
    private SpriteRenderer beeRenderer;
    public float TimeWait;
    public float SpeedMove;
    bool ismove;


    // Use this for initialization
    void Start()
    {
        beeRenderer = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (beeRenderer.isVisible)
        {
            BeeMove();
        }

    }
    void BeeMove()
    {

        StartCoroutine(anima());
        if (transform.position.y < -5f)
        {
            Destroy(gameObject);
        }
    }
    IEnumerator anima()
    {
        yield return new WaitForSeconds(1);
        anim.SetBool("Play", ismove);
        transform.Translate(new Vector2(-1, -1) * Time.deltaTime * SpeedMove);
    }
}
