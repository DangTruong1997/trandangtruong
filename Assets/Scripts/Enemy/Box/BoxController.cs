﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxController : MonoBehaviour {
    
    public void BoxDie()
    {
        gameObject.SetActive(false);
        LV1Controller.Instance._score += 3;
    }
}
