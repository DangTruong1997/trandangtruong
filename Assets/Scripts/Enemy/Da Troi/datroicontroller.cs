﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class datroicontroller : MonoBehaviour
{

    public float SpeedDaTroiMove;
    public float SpeedMove;
    private SpriteRenderer DaTroiRenderer;
    public bool CheckEnterPlayer;

    private void Start()
    {
        SpeedDaTroiMove = SpeedMove;
        CheckEnterPlayer = false;
        DaTroiRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if(DaTroiRenderer.isVisible && CheckEnterPlayer == true)
        {
            SpeedDaTroiMove = SpeedMove;
            transform.Translate(Vector3.right * SpeedDaTroiMove);
        }
    }
       

    private void OnCollisionEnter2D(Collision2D coll)
    {
        if(coll.gameObject.tag == "Map")
        {
            SpeedDaTroiMove = 0;
            CheckEnterPlayer = false;
        }
    }
}
