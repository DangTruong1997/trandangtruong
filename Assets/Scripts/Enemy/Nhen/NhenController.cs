﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NhenController : MonoBehaviour
{

    public Animator anim;
    private SpriteRenderer NhenRenderer;
    public float SpeedMove;
    private bool isTurned;
    // Use this for initialization
    void Start()
    {
        isTurned = false;
        NhenRenderer = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if(NhenRenderer.isVisible)
        {
            anim.Play("NhenMove");
            transform.Translate(Vector3.left * Time.deltaTime * SpeedMove);
        }
    }

    private void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Box"&&!isTurned)
        {
            isTurned = true;
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y+180, transform.eulerAngles.z);
        }
    }
    private void OnTriggerExit2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Box" )
        {
            isTurned = false;
        }
    }
    public void NhenDie()
    {
        Destroy(gameObject);
        LV1Controller.Instance._score += 10;
    }
    

}
