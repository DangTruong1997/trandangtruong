﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneGameController : MonoBehaviour {

    public static SceneGameController Instance;
    public Scene Level1;
    public GameObject PlayPopup;
    public GameObject MapPopup;
    public GameObject GameOver;

    private void Awake()
    {
        Instance = this;
    }

    // Use this for initialization
    void Start () {
        PlayPopup.SetActive(true);
        MapPopup.SetActive(false);
        DontDestroyOnLoad(gameObject);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    public void BottonPlay()
    {
        PlayPopup.SetActive(false);
        MapPopup.SetActive(true);
    }

    public void bottonLevel1()
    {
        SceneManager.LoadSceneAsync("Level1");
    }

    public void selectMap()
    {
        GameOver.SetActive(false);
        MapPopup.SetActive(true);
    }
}
